#ifndef H_DEBUG
#define H_DEBUG

#include <stdarg.h>

void debug(const char* format, ...);
void debug_separator(void);

#endif
