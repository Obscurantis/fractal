#ifndef H_EVENT
#define H_EVENT

#include "fractal.h"

void event_loop(struct fractal*);

#endif
