#ifndef H_COLOR
#define H_COLOR

#include <stdint.h>

struct color
{
  uint8_t r;
  uint8_t g;
  uint8_t b;
};

#endif
